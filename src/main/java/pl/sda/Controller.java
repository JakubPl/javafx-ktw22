package pl.sda;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Window;

import java.util.Optional;
import java.util.Random;


public class Controller {

    @FXML
    private Canvas canvas;
    @FXML
    private Button button;

    public void initialize() {
    }

    public void buttonClick(ActionEvent actionEvent) {
        Window owner = canvas.getScene().getWindow();
        TextInputDialog alert = new TextInputDialog();
        alert.setTitle("Trojkaty");
        alert.setHeaderText(null);
        alert.setContentText("Podaj ilosc trojkatow:");
        alert.initOwner(owner);

        alert.showAndWait()
                .map(amountString -> Integer.valueOf(amountString))
                .ifPresent(amount -> drawTriangles(amount));

        //metoda 1 (mniej zalecana)
        /*if(result.isPresent()) {
            String amountOfTrianglesToDrawString = result.get();
            Integer amountOfTrianglesToDraw = Integer.valueOf(amountOfTrianglesToDrawString);
            //metoda do rysowania trojkatow
            // drawTriangles(amountOfTrianglesToDraw)
        }
        //metoda 2 (zalecana)
        result.map(amountString -> Integer.valueOf(amountString))
                .ifPresent(amount -> drawTriangles(amount));*/
    }

    private void drawTriangles(int amount) {
        for(int i = 0; i < amount; i ++) {
            drawTriangle();
        }
    }

    private void drawTriangle() {
        final GraphicsContext gc = canvas.getGraphicsContext2D();

        final Random randomGenerator = new Random();
        int x1 = randomGenerator.nextInt(600);
        int x2 = randomGenerator.nextInt(600);
        int x3 = randomGenerator.nextInt(600);
        int y1 = randomGenerator.nextInt(600);
        int y2 = randomGenerator.nextInt(600);
        int y3 = randomGenerator.nextInt(600);

        //jak zmienic kolor na swoj wlasny
        int red = randomGenerator.nextInt(255);
        int green = randomGenerator.nextInt(255);
        int blue = randomGenerator.nextInt(255);

        gc.setFill(Color.rgb(red, green, blue));
        // jak narysowac trojkat:
        gc.fillPolygon(new double[]{x1, x2, x3},
                new double[]{y1, y2, y3}, 3);
    }

    public void clearCanvas(ActionEvent actionEvent) {
        final GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, 600, 600);
    }
}
